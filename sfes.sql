-- Table: public.sup_flux_es

-- DROP TABLE public.sup_flux_es;

CREATE TABLE public.sup_flux_es
(
  id integer NOT NULL,
  idflux character varying(50),
  localisation character varying(50),
  sens character varying(50),
  "precision" character varying(50),
  etat character varying(50),
  fichiererreur character varying(255),
  messageerreur character varying(255),
  datereception timestamp without time zone,  
  CONSTRAINT sup_flux_es_pkey PRIMARY KEY (id)
)


