<%@page import="com.svang.model.Sfes"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SFES</title>
</head>
<body>
	<div>
        <div>
            <h1>Supervision des Flux Entrant et Sortant</h1>
            <h2>Flux reçu:</h2>
            <strong><a href="supervision">Rafraichir la liste</a></strong><br><br>
            
            <table border="1">
            	<tr>
 					<td>Id</td>
 					<td>Id flux</td>   
 					<td>Localisation</td>
 					<td>Sens</td>
 					<td>Précision</td>      
 					<td>Etat</td>
 					<td>Message d'erreurs</td>
 					<td>Fichier d'erreurs</td>
 					<td>Date de réception</td>  	  	
            	</tr>
            
            <%
            	List<Sfes> fluxList = (List<Sfes>)request.getAttribute("fluxList");
            	int size = fluxList.size();
            	System.out.println(size);
            	for(Sfes sfes:fluxList){       		
            	
            %>
            
            	<tr>
 					
 					<%if("NTRANSFERE".equals(sfes.getEtat())) {%>   
 						<td><b><font color="red"><%= sfes.getId()%></font></b></td>
	 					<td><b><font color="red"><%= sfes.getIdFlux()%></font></b></td> 
	 					
	 					<td><b><font color="red"><%= sfes.getLocalisation()%></font></b></td>   
	 					<td><b><font color="red"><%= sfes.getSens()%></font></b></td>   
	 					<td><b><font color="red"><%= sfes.getPrecision()%></font></b></td> 
 						<td><b><font color="red"><%= sfes.getEtat()%></font></b></td>
 						<td><b><font color="red"><%= sfes.getMessageErreur()%></font></b></td>
 						<td><b><font color="red"><%= sfes.getFichierErreur()%></font></b></td>
 						<td><b><font color="red"><%= sfes.getDateReception()%></font></b></td> 
 						   
 					<%}else{ %>
	 					<td><%= sfes.getId()%></td>
	 					<td><%= sfes.getIdFlux()%></td> 
	 					
	 					<td><%= sfes.getLocalisation()%></td>   
	 					<td><%= sfes.getSens()%></td>   
	 					<td><%= sfes.getPrecision()%></td> 
 						<td><%= sfes.getEtat()%></td>
 						<td></td>
 						<td></td>
 						<td><%= sfes.getDateReception()%></td>
 					<%} %>
 					     	
            	</tr>
             	
			<%} %> 
			</table>
			           
        </div>
    </div>
</body>
</html>