package com.svang.controller;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.svang.model.Sfes;
import com.svang.service.SfesService;

@Controller
public class SfesController {
	
	@Autowired
	private SfesService sfesService;
	
	@RequestMapping("/sfes")
	public String supervision(Map<String, Object> model) {
		
		List<Sfes> list = sfesService.findAll();
		
		
		model.put("fluxList", list);
		return "sfes";
	}
    
}
