package com.svang.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "sup_flux_es")
@Entity
public class Sfes {
	
	@Id
	private int id;
	@Column(name = "idflux")
	private String idFlux;
	@Column
	private String localisation;
	@Column
	private String sens;
	@Column
	private String precision;
	@Column
	private String etat;
	@Column(name = "messageerreur")
	private String messageErreur;
	@Column(name = "fichiererreur")
	private String fichierErreur;
	@Column(name = "datereception")
	private Date dateReception;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getIdFlux() {
		return idFlux;
	}
	public void setIdFlux(String idFlux) {
		this.idFlux = idFlux;
	}
	public String getLocalisation() {
		return localisation;
	}
	public void setLocalisation(String localisation) {
		this.localisation = localisation;
	}
	public String getSens() {
		return sens;
	}
	public void setSens(String sens) {
		this.sens = sens;
	}
	public String getPrecision() {
		return precision;
	}
	public void setPrecision(String precision) {
		this.precision = precision;
	}
	public String getEtat() {
		return etat;
	}
	public void setEtat(String etat) {
		this.etat = etat;
	}
	public Date getDateReception() {
		return dateReception;
	}
	public void setDateReception(Date dateReception) {
		this.dateReception = dateReception;
	}
	public String getFichierErreur() {
		return fichierErreur;
	}
	public void setFichierErreur(String fichierErreur) {
		this.fichierErreur = fichierErreur;
	}
	public String getMessageErreur() {
		return messageErreur;
	}
	public void setMessageErreur(String messageErreur) {
		this.messageErreur = messageErreur;
	}
	
		
}
