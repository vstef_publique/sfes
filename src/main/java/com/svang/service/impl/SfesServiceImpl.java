package com.svang.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.svang.dao.SfesDao;
import com.svang.model.Sfes;
import com.svang.service.SfesService;

@Service
public class SfesServiceImpl implements SfesService {
	@Autowired
	private SfesDao sfesDao;
	@Override
	public List<Sfes> findAll() {
		return Lists.newArrayList(sfesDao.findAll());
	}

}
