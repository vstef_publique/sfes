package com.svang.dao;

import org.springframework.data.repository.CrudRepository;

import com.svang.model.Sfes;

public interface SfesDao extends CrudRepository<Sfes, Integer>{
}
