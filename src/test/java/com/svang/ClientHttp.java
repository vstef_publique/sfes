package com.svang;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class ClientHttp {

	private static String boundary;
	private static final String LINE_FEED = "\r\n";
	private static HttpURLConnection httpConn;
	private static String charset = "UTF-8";
	private static OutputStream outputStream;
	private static PrintWriter writer;
	private static String json = "{\n" + 
			"      \n" + 
			"        \"nomVendeur\": \"post\",\n" + 
			"        \"email\": \"asskicker@ynov.com\",\n" + 
			"        \"mdp\": \"asskicker\",\n" + 
			"        \"titre\": \"Image DBZ\",\n" + 
			"        \"localisation\": \"Lyon\",\n" + 
			"        \"categorie\": \"Vêtements\",\n" + 
			"        \"prix\": 35,\n" + 
			"        \"description\": \"Bonjour,\\nJe vends des polos Lacoste neufs.\\n35 euros l'unité.\\nBonne journée.\"\n" + 
			"        }";
	
	private static File file = new File("/home/bardock/Images/1.png");
	
	public static void main(String[] args) {
		
		try {
			init("http://139.99.98.119:8080/saveAnnonce");
			addJson("annonce", json);
			addFilePart("file", file);
			int response = finish();
            
            System.out.println("SERVER REPLIED:" + response);
            
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}
	
	public static void init(String requestURL) throws Exception {
        // creates a unique boundary based on time stamp
        boundary = "===" + System.currentTimeMillis() + "===";
         
        URL url = new URL(requestURL);
        httpConn = (HttpURLConnection) url.openConnection();
        httpConn.setUseCaches(false);
        httpConn.setDoOutput(true); // indicates POST method
        httpConn.setDoInput(true);
        httpConn.setRequestProperty("Content-Type",
                "multipart/form-data; boundary=" + boundary);
        outputStream = httpConn.getOutputStream();
        writer = new PrintWriter(new OutputStreamWriter(outputStream, charset),
                true);
	}
	
	 public static void addJson(String name, String value) {
        writer.append("--" + boundary).append(LINE_FEED);
        writer.append("Content-Disposition: form-data; name=\"" + name + "\"")
                .append(LINE_FEED);
        writer.append("Content-Type: application/json; charset=" + charset).append(
                LINE_FEED);
        writer.append(LINE_FEED);
        writer.append(value).append(LINE_FEED);
        writer.flush();
    }
	 
	 public static void addFilePart(String fieldName, File uploadFile)
	            throws Exception {
        String fileName = uploadFile.getName();
        writer.append("--" + boundary).append(LINE_FEED);
        writer.append(
                "Content-Disposition: form-data; name=\"" + fieldName
                        + "\"; filename=\"" + fileName + "\"")
                .append(LINE_FEED);
        writer.append(
                "Content-Type: "
                        + URLConnection.guessContentTypeFromName(fileName))
                .append(LINE_FEED);
        writer.append("Content-Transfer-Encoding: binary").append(LINE_FEED);
        writer.append(LINE_FEED);
        writer.flush();
 
        FileInputStream inputStream = new FileInputStream(uploadFile);
        byte[] buffer = new byte[4096];
        int bytesRead = -1;
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, bytesRead);
        }
        outputStream.flush();
        inputStream.close();
         
        writer.append(LINE_FEED);
        writer.flush();    
    }
	 
    public static void addHeaderField(String name, String value) {
        writer.append(name + ": " + value).append(LINE_FEED);
        writer.flush();
    }
	     
    public static int finish() throws IOException {
 
        writer.append(LINE_FEED).flush();
        writer.append("--" + boundary + "--").append(LINE_FEED);
        writer.close();
 
        // checks server's status code first
        return httpConn.getResponseCode();
    }
	
	
	

}
